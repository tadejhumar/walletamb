from web3 import (
    Web3,
    HTTPProvider
)

from eth_account import Account


class AMB:
    chain_id = 0x414e

    def __init__(self):
        self.connection = Web3(HTTPProvider("https://network.ambrosus.io"))

    """ Get connection status to the RPC"""
    def is_connected(self):
        return self.connection.isConnected()

    """ Get the balance on the provided account"""
    def get_balance(self, public_key):
        return self.connection.fromWei(self.connection.eth.getBalance(public_key), 'ether')

    """ Get the chain ID and print a warning if it does not match the AMB one."""
    def get_chain_id(self):
        cid = self.connection.eth.chain_id
        if cid != self.chain_id:
            print("Warning this is not AMB chain ID: {} should be {}".format(cid, self.chain_id))
        return cid

    """ Get the nonce for the provided address."""
    def get_nonce(self, public_key):
        return self.connection.eth.get_transaction_count(public_key)

    """ Make a transaction with desired value. Fails if the balance is not sufficient."""
    def send_amb(self, public_key, private_key, receiver, value):
        if value >= self.get_balance(public_key):
            raise ValueError("You don't have enough $!")

        transaction = {
            'to': Web3.toChecksumAddress(receiver),
            'value': self.connection.toWei(value, 'ether'),
            'gas': 21000,
            'nonce': self.get_nonce(public_key),
            'gasPrice': 1,
            'chainId': hex(self.chain_id)
        }

        signed_tx = Account.signTransaction(transaction, private_key)
        tx_hash = self.connection.eth.sendRawTransaction(signed_tx.rawTransaction)
        return tx_hash
