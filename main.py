from BlockChainAPI import AMB
import argparse
from hexbytes import HexBytes

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('fromAddr', type=str, help='Your public address.')
    parser.add_argument('privKey', type=str, help='Your private key.')
    parser.add_argument('toAddr', type=str, help='Receivers public address.')
    parser.add_argument('amount', type=int, help='Amount of AMB to send.')

    args = parser.parse_args()
    print(HexBytes(AMB().send_amb(args.fromAddr, args.privKey, args.toAddr, args.amount)).hex())



